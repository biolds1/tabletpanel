This repository contains a lightweight Python/GTK3 panel that can be used on Linux tablets to change the backlight and the volume.

![Screenshot](screen.png)

To install use it on Debian:

```
apt-get install python3-gi
cp tabletpanel /usr/local/bin/
```

The user opening the X session also needs to be put in the `video` to have access to backlight device, and this udev rule should be added:

`/etc/udev/rules.d/backlight.rules`
```
ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chgrp video $sys$devpath/brightness", RUN+="/bin/chmod g+w $sys$devpath/brightness"
```

